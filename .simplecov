SimpleCov.start do
  enable_coverage :branch
  primary_coverage :branch
  minimum_coverage 100
  add_filter "/vendor/"
end

# vim:filetype=ruby
