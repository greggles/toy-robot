require 'simplecov'
require 'toy_robot'

RSpec.describe ToyRobot do
  describe '.cli' do
    subject { ToyRobot.cli(input: StringIO.new(input)) }

    context "with a basic move (example a)" do
      # •••••
      # •••••
      # •••••
      # X••••
      # O••••
      let(:input) do
        '
        PLACE 0,0,NORTH
        MOVE
        REPORT
        '
      end

      it "outputs the expected result" do
        expect{subject}.to output("0,1,NORTH\n").to_stdout
      end
    end

    context "with a basic turn (example b)" do
      # •••••
      # •••••
      # •••••
      # •••••
      # O••••
      let(:input) do
        '
        PLACE 0,0,NORTH
        LEFT
        REPORT
        '
      end

      it "outputs the expected result" do
        expect{subject}.to output("0,0,WEST\n").to_stdout
      end
    end

    context "with a move, move, turn, move (example c)" do
      # •••••
      # •••X•
      # •OXX•
      # •••••
      # •••••
      let(:input) do
        '
        PLACE 1,2,EAST
        MOVE
        MOVE
        LEFT
        MOVE
        REPORT
        '
      end

      it "outputs the expected result" do
        expect{subject}.to output("3,3,NORTH\n").to_stdout
      end
    end

    context "with no input" do
      let(:input) { '' }
      it "does not output anything" do
        expect{subject}.not_to output.to_stdout
      end
    end

    context "with a complete set of moves" do
      # •••••
      # •••••
      # •X•••
      # XX•••
      # O••••
      let(:input) do
        '
        PLACE 0,0,NORTH
        MOVE
        RIGHT
        MOVE
        LEFT
        MOVE
        REPORT
        '
      end

      it "outputs the expected result" do
        expect{subject}.to output("1,2,NORTH\n").to_stdout
      end
    end

    context "when trying to move off the board" do
      # •••••
      # •••••
      # •••••
      # •••••
      # O••••
      let(:input) do
        '
        PLACE 0,0,SOUTH
        MOVE
        REPORT
        '
      end

      it "outputs the expected result" do
        expect{subject}.to output("0,0,SOUTH\n").to_stdout
      end
    end

    context "commands without placing the robot" do
      let(:input) do
        '
        MOVE
        RIGHT
        MOVE
        LEFT
        MOVE
        REPORT
        '
      end

      it "does not output anything" do
        expect{subject}.not_to output.to_stdout
      end
    end

    context "place at an invalid position" do
      let(:input) do
        '
        PLACE 100,0,SOUTH
        REPORT
        '
      end

      it "does not output anything" do
        expect{subject}.not_to output.to_stdout
      end
    end
  end
end
