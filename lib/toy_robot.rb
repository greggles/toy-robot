# A toy robot coding exercise
module ToyRobot
  # Command line interface for the toy robot program. Input is a Ruby IO
  # object, such as a StringIO, $stdin, or ARGF.
  def self.cli(input:)
    bot = Bot.new
    tabletop = Tabletop.new(width: 5, height: 5)
    world = World.new(bot:, tabletop:)
    input.each_line { |line| world.process_command(line) }
  end

  # The top level model of the situation. A robot (Bot object) and a tabletop
  # (Tabletop object).
  class World
    def initialize(bot:, tabletop:)
      @bot = bot
      @tabletop = tabletop
    end

    # Process one of the bot commands (e.g. "MOVE")
    def process_command(command)
      command = command.strip
      case [@bot.placed?, command]
      when [true, 'MOVE']
        @bot.move! if @tabletop.valid_position?(@bot.next_position)
      when [true, 'LEFT']
        @bot.turn_left!
      when [true, 'RIGHT']
        @bot.turn_right!
      when [true, 'REPORT']
        @bot.report
      else
        process_place_command(command)
      end
    end

    private

    PLACE_REGEXP = /^PLACE (?<x>\d+),(?<y>\d+),(?<facing>NORTH|SOUTH|EAST|WEST)$/
    private_constant :PLACE_REGEXP

    def process_place_command(command)
      if m = command.match(PLACE_REGEXP)
        position = [m[:x], m[:y]].map(&:to_i)
        facing = m[:facing]
        @bot.place!(position:, facing:) if @tabletop.valid_position?(position)
      end
    end
  end

  # A robot with a position and a facing direction. It can be issued commands
  # to initialize itself and move around on Cartesian coordinates.
  class Bot
    def initialize
      @position = nil
      @facing = nil
    end

    # Place the bot at position and facing. e.g. `position: [0, 1], facing: "NORTH"`
    def place!(position:, facing:)
      @position = position
      @facing = facing
    end

    def placed?
      !@position.nil? && !@facing.nil?
    end

    def turn_left!
      turn!(-1)
    end

    def turn_right!
      turn!(+1)
    end

    def next_position
      @position.zip(DELTA_POSITION[@facing]).map(&:sum)
    end

    def move!
      @position = next_position
    end

    def report
      puts "#{@position.join(',')},#{@facing}"
    end

    private

    # Map cardinal direction into 90 degrees separated clockwise integers
    CLOCKWISE_INDEX = {
      'NORTH' => 0,
      'EAST' => 1,
      'SOUTH' => 2,
      'WEST' => 3,
    }.freeze
    private_constant :CLOCKWISE_INDEX

    # The cardinal direction, given a clockwise index from the mapping above
    CARDINAL_FACING = CLOCKWISE_INDEX.invert.freeze
    private_constant :CARDINAL_FACING

    # The [dx, dy] for the next MOVE, given a cardinal direction
    DELTA_POSITION = {
      'NORTH' => [0, 1],
      'EAST' => [1, 0],
      'SOUTH' => [0, -1],
      'WEST' => [-1, 0],
    }.freeze
    private_constant :DELTA_POSITION

    def turn!(delta)
      # Use modulo to ensure the result is on the range 0..n
      clockwise_index = CLOCKWISE_INDEX[@facing]
      clockwise_index = (clockwise_index + delta) % CLOCKWISE_INDEX.size
      @facing = CARDINAL_FACING[clockwise_index]
    end
  end

  # A tabletop with dimensions `width` x `height`
  #
  # Coordinate system
  #
  #     height
  #     ^
  #     |
  #     (0,0) --> width
  #
  class Tabletop
    def initialize(width:, height:)
      @width = width
      @height = height
    end

    def valid_position?((x, y))
      # a...b is exclusive, it is the range [a, b)
      (0...@width).include?(x) && (0...@height).include?(y)
    end
  end
end
